package com.affymetrix.genometryImpl.thread;

public interface WaitHelperI {
	public Boolean waitForAll();
}
