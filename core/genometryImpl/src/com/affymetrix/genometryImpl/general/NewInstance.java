package com.affymetrix.genometryImpl.general;

/**
 *
 * @author hiralv
 */
public interface NewInstance<T> {
	public T newInstance();
}
