package com.affymetrix.igb.shared;

import java.util.List;

public interface TrackListProvider {
	public List<TierGlyph> getTrackList();
}
