package com.affymetrix.genoviz.swing.recordplayback;

public interface JRPWidget {
	public String getId();
	public boolean consecutiveOK();
}
