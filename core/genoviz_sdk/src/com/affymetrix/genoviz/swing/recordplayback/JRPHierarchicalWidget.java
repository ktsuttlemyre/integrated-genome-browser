package com.affymetrix.genoviz.swing.recordplayback;

public interface JRPHierarchicalWidget extends JRPWidget {
	public SubRegionFinder getSubRegionFinder(String subId);
}
