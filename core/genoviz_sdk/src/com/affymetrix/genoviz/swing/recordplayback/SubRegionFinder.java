package com.affymetrix.genoviz.swing.recordplayback;

import java.awt.Rectangle;

public interface SubRegionFinder {
	public Rectangle getRegion();
}
