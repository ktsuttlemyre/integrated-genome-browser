package com.affymetrix.genoviz.swing.recordplayback;

public interface JRPWidgetDecorator {
	public void widgetAdded(JRPWidget widget);
}
