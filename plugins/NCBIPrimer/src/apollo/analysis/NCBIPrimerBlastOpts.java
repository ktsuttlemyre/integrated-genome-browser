package apollo.analysis;

/**
 *
 * @author hiralv
 */
public interface NCBIPrimerBlastOpts {
	
	public RemotePrimerBlastNCBI.PrimerBlastOptions getOptions();
}
