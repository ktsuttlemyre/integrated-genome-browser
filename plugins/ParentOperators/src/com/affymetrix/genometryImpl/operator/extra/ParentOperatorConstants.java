package com.affymetrix.genometryImpl.operator.extra;

import java.util.ResourceBundle;

/**
 *
 * @author hiralv
 */
public interface ParentOperatorConstants {
	
	public static final ResourceBundle BUNDLE = ResourceBundle.getBundle("parentoperators");
}
