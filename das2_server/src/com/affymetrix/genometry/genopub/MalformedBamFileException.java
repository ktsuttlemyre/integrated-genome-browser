package com.affymetrix.genometry.genopub;


public class MalformedBamFileException extends Exception {

  public MalformedBamFileException(String message) {
    super(message);
  }
}
