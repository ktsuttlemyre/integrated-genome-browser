package com.affymetrix.genometry.genopub;


public class PropertyOption  {
    
    private Integer idPropertyOption;
    private String  name;
    private String  isActive;
    private Integer sortOrder;
    private Integer idProperty;
    
    public Integer getIdPropertyOption() {
      return idPropertyOption;
    }
    public void setIdPropertyOption(Integer idPropertyOption) {
      this.idPropertyOption = idPropertyOption;
    }
    public String getName() {
      return name;
    }
    public void setName(String name) {
      this.name = name;
    }
    public String getIsActive() {
      return isActive;
    }
    public void setIsActive(String isActive) {
      this.isActive = isActive;
    }
    public Integer getSortOrder() {
      return sortOrder;
    }
    public void setSortOrder(Integer sortOrder) {
      this.sortOrder = sortOrder;
    }
    public Integer getIdProperty() {
      return idProperty;
    }
    public void setIdProperty(Integer idProperty) {
      this.idProperty = idProperty;
    }
    
    
  
}
