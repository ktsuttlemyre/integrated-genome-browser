package com.affymetrix.genometry.genopub;


public class BulkFileUploadException extends Exception {

  public BulkFileUploadException(String message) {
    super(message);
  }
}
